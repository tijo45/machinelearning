// Author: Jacek Aleksander Gruca

package mltp {

import scala.collection.mutable._

object PatternProcessor {

	type Pattern = MachineLearner.Pattern
	type PrecisionType = MachineLearner.PrecisionType

	def getPercentChange(from: PrecisionType, to: PrecisionType): PrecisionType = {

		try {
			val x = 100.00 * (to - from) / (if (from < 0) -from else from)
			if (x == 0.0) {
				0.000000001
			}
			else {
				x
			}
		} catch {
			case e: Exception =>
				0.0001
		}
	}

	def storePatterns(averagePrices: ArrayBuffer[PrecisionType],
	                  patterns: ArrayBuffer[Pattern],
	                  performances: ArrayBuffer[PrecisionType]) = {

		//println("Store patterns ----------------------- ")

		val startTimeMillis = System.currentTimeMillis()

		val x = averagePrices.length - 30
		var y = 31

		//println("Start loop ----------------------- ")
		while (y < x) {

			//println("y=" + y + ", x=" + x)

			val pattern = new ArrayBuffer[PrecisionType]

			//println("from="+averagePrices(y - 30))
			//println("to="+averagePrices(y - 29))
			//println("getPercentChange"+getPercentChange(averagePrices(y - 30), averagePrices(y - 29)))
			val p1 = getPercentChange(averagePrices(y - 30), averagePrices(y - 29))
			val p2 = getPercentChange(averagePrices(y - 30), averagePrices(y - 28))
			val p3 = getPercentChange(averagePrices(y - 30), averagePrices(y - 27))
			val p4 = getPercentChange(averagePrices(y - 30), averagePrices(y - 26))
			val p5 = getPercentChange(averagePrices(y - 30), averagePrices(y - 25))
			val p6 = getPercentChange(averagePrices(y - 30), averagePrices(y - 24))
			val p7 = getPercentChange(averagePrices(y - 30), averagePrices(y - 23))
			val p8 = getPercentChange(averagePrices(y - 30), averagePrices(y - 22))
			val p9 = getPercentChange(averagePrices(y - 30), averagePrices(y - 21))
			val p10 = getPercentChange(averagePrices(y - 30), averagePrices(y - 20))

			val p11 = getPercentChange(averagePrices(y - 30), averagePrices(y - 19))
			val p12 = getPercentChange(averagePrices(y - 30), averagePrices(y - 18))
			val p13 = getPercentChange(averagePrices(y - 30), averagePrices(y - 17))
			val p14 = getPercentChange(averagePrices(y - 30), averagePrices(y - 16))
			val p15 = getPercentChange(averagePrices(y - 30), averagePrices(y - 15))
			val p16 = getPercentChange(averagePrices(y - 30), averagePrices(y - 14))
			val p17 = getPercentChange(averagePrices(y - 30), averagePrices(y - 13))
			val p18 = getPercentChange(averagePrices(y - 30), averagePrices(y - 12))
			val p19 = getPercentChange(averagePrices(y - 30), averagePrices(y - 11))
			val p20 = getPercentChange(averagePrices(y - 30), averagePrices(y - 10))

			val p21 = getPercentChange(averagePrices(y - 30), averagePrices(y - 9))
			val p22 = getPercentChange(averagePrices(y - 30), averagePrices(y - 8))
			val p23 = getPercentChange(averagePrices(y - 30), averagePrices(y - 7))
			val p24 = getPercentChange(averagePrices(y - 30), averagePrices(y - 6))
			val p25 = getPercentChange(averagePrices(y - 30), averagePrices(y - 5))
			val p26 = getPercentChange(averagePrices(y - 30), averagePrices(y - 4))
			val p27 = getPercentChange(averagePrices(y - 30), averagePrices(y - 3))
			val p28 = getPercentChange(averagePrices(y - 30), averagePrices(y - 2))
			val p29 = getPercentChange(averagePrices(y - 30), averagePrices(y - 1))
			val p30 = getPercentChange(averagePrices(y - 30), averagePrices(y))

			val outcomeRange = averagePrices.slice(y + 20, y + 30)
			val currentPoint = averagePrices(y)

			var avgOutcome = 0.0
			try {
				avgOutcome = outcomeRange.sum / outcomeRange.length
			} catch {
				case e: Exception =>
					println(e)
			}
			val futureOutcome = getPercentChange(currentPoint, avgOutcome)

			pattern.append(p1)
			pattern.append(p2)
			pattern.append(p3)
			pattern.append(p4)
			pattern.append(p5)
			pattern.append(p6)
			pattern.append(p7)
			pattern.append(p8)
			pattern.append(p9)
			pattern.append(p10)

			pattern.append(p11)
			pattern.append(p12)
			pattern.append(p13)
			pattern.append(p14)
			pattern.append(p15)
			pattern.append(p16)
			pattern.append(p17)
			pattern.append(p18)
			pattern.append(p19)
			pattern.append(p20)

			pattern.append(p21)
			pattern.append(p22)
			pattern.append(p23)
			pattern.append(p24)
			pattern.append(p25)
			pattern.append(p26)
			pattern.append(p27)
			pattern.append(p28)
			pattern.append(p29)
			pattern.append(p30)

			patterns += pattern
			performances += futureOutcome
			//println("pattern="+pattern)
			//println("futureOutcome="+futureOutcome)

			y += 1
		}
		//println("End loop ----------------------- ")

		//println("performancesSum=" + performances.sum)

		var sum = 0.0
		patterns.foreach(pattern => {
			sum += pattern.sum
		})
		//println("patternsSum=" + sum)

		val totalTimeMillis = System.currentTimeMillis() - startTimeMillis
		println("Pattern storing took: " + totalTimeMillis / 1000.0)

	}

	def buildTicks(averagePrices: ArrayBuffer[PrecisionType]): Pattern = {

		//println("Build ticks ----------------------- ")

		val startTime = System.currentTimeMillis()

		//println(averagePrices.mkString("\n"))

		val cp1 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 30))
		val cp2 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 29))
		val cp3 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 28))
		val cp4 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 27))
		val cp5 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 26))
		val cp6 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 25))
		val cp7 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 24))
		val cp8 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 23))
		val cp9 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 22))
		val cp10 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 21))

		val cp11 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 20))
		val cp12 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 19))
		val cp13 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 18))
		val cp14 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 17))
		val cp15 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 16))
		val cp16 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 15))
		val cp17 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 14))
		val cp18 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 13))
		val cp19 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 12))
		val cp20 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 11))

		val cp21 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 10))
		val cp22 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 9))
		val cp23 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 8))
		val cp24 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 7))
		val cp25 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 6))
		val cp26 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 5))
		val cp27 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 4))
		val cp28 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 3))
		val cp29 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 2))
		val cp30 = getPercentChange(averagePrices(averagePrices.length - 31), averagePrices(averagePrices.length - 1))

		val ticks: Pattern = new ArrayBuffer[PrecisionType]

		ticks.append(cp1)
		ticks.append(cp2)
		ticks.append(cp3)
		ticks.append(cp4)
		ticks.append(cp5)
		ticks.append(cp6)
		ticks.append(cp7)
		ticks.append(cp8)
		ticks.append(cp9)
		ticks.append(cp10)
		ticks.append(cp11)
		ticks.append(cp12)
		ticks.append(cp13)
		ticks.append(cp14)
		ticks.append(cp15)
		ticks.append(cp16)
		ticks.append(cp17)
		ticks.append(cp18)
		ticks.append(cp19)
		ticks.append(cp20)
		ticks.append(cp21)
		ticks.append(cp22)
		ticks.append(cp23)
		ticks.append(cp24)
		ticks.append(cp25)
		ticks.append(cp26)
		ticks.append(cp27)
		ticks.append(cp28)
		ticks.append(cp29)
		ticks.append(cp30)

		println("CurrentPattern took: " + (System.currentTimeMillis() - startTime) / 1000.0)

		ticks
	}

	def matchPatterns(ticks: Pattern,
	                  patterns: ArrayBuffer[Pattern],
	                  performances: ArrayBuffer[PrecisionType],
	                  allAveragePrices: ArrayBuffer[PrecisionType],
	                  intersect: Int,
	                  accuracies: ArrayBuffer[PrecisionType]): ArrayBuffer[PrecisionType] = {

		//println("Match patterns ----------------------- ")

		val startTime = System.currentTimeMillis()

		val predictedOutcomes = new ArrayBuffer[PrecisionType]
		val matchingPatterns = new ArrayBuffer[Pattern]
		var patternFound: Boolean = false
		val howSims: ArrayBuffer[PrecisionType] = new ArrayBuffer[PrecisionType]()

		var value: PrecisionType = 0
		var sim1, sim2, sim3, sim4, sim5, sim6, sim7, sim8, sim9, sim10,
		sim11, sim12, sim13, sim14, sim15, sim16, sim17, sim18, sim19, sim20,
		sim21, sim22, sim23, sim24, sim25, sim26, sim27, sim28, sim29, sim30: Double = 0

		//var sum = 0.0
		//patterns.foreach(pattern => {
		//	sum += pattern.sum
		//})
		//println("patternsSum2=" + sum)

		patterns.foreach(pattern => {
			//println("patternSum="+pattern.sum)
			value = getPercentChange(pattern.head, ticks.head)
			sim1 = 100.00 - (if (value < 0) -value else value)
			if (sim1 > 50) {
				value = getPercentChange(pattern(1), ticks(1))
				sim2 = 100.00 - (if (value < 0) -value else value)
				if (sim2 > 50) {
					value = getPercentChange(pattern(2), ticks(2))
					sim3 = 100.00 - (if (value < 0) -value else value)
					if (sim3 > 50) {
						value = getPercentChange(pattern(3), ticks(3))
						sim4 = 100.00 - (if (value < 0) -value else value)
						if (sim4 > 50) {
							value = getPercentChange(pattern(4), ticks(4))
							sim5 = 100.00 - (if (value < 0) -value else value)
							if (sim5 > 50) {
								value = getPercentChange(pattern(5), ticks(5))
								sim6 = 100.00 - (if (value < 0) -value else value)
								if (sim6 > 50) {
									value = getPercentChange(pattern(6), ticks(6))
									sim7 = 100.00 - (if (value < 0) -value else value)
									if (sim7 > 50) {
										value = getPercentChange(pattern(7), ticks(7))
										sim8 = 100.00 - (if (value < 0) -value else value)
										if (sim8 > 50) {
											value = getPercentChange(pattern(8), ticks(8))
											sim9 = 100.00 - (if (value < 0) -value else value)
											if (sim9 > 50) {
												value = getPercentChange(pattern(9), ticks(9))
												sim10 = 100.00 - (if (value < 0) -value else value)
												if (sim10 > 50) {
													value = getPercentChange(pattern(10), ticks(10))
													sim11 = 100.00 - (if (value < 0) -value else value)
													if (sim11 > 50) {
														value = getPercentChange(pattern(11), ticks(11))
														sim12 = 100.00 - (if (value < 0) -value else value)
														if (sim12 > 50) {
															value = getPercentChange(pattern(12), ticks(12))
															sim13 = 100.00 - (if (value < 0) -value else value)
															if (sim13 > 50) {
																value = getPercentChange(pattern(13), ticks(13))
																sim14 = 100.00 - (if (value < 0) -value else value)
																if (sim14 > 50) {
																	value = getPercentChange(pattern(14), ticks(14))
																	sim15 = 100.00 - (if (value < 0) -value else value)
																	if (sim15 > 50) {
																		value = getPercentChange(pattern(15), ticks(15))
																		sim16 = 100.00 - (if (value < 0) -value else value)
																		if (sim16 > 50) {
																			value = getPercentChange(pattern(16), ticks(16))
																			sim17 = 100.00 - (if (value < 0) -value else value)
																			if (sim17 > 50) {
																				value = getPercentChange(pattern(17), ticks(17))
																				sim18 = 100.00 - (if (value < 0) -value else value)
																				if (sim18 > 50) {
																					value = getPercentChange(pattern(18), ticks(18))
																					sim19 = 100.00 - (if (value < 0) -value else value)
																					if (sim19 > 50) {
																						value = getPercentChange(pattern(19), ticks(19))
																						sim20 = 100.00 - (if (value < 0) -value else value)
																						if (sim20 > 50) {
																							value = getPercentChange(pattern(20), ticks(20))
																							sim21 = 100.00 - (if (value < 0) -value else value)
																							if (sim21 > 50) {
																								value = getPercentChange(pattern(21), ticks(21))
																								sim22 = 100.00 - (if (value < 0) -value else value)
																								if (sim22 > 50) {
																									value = getPercentChange(pattern(22), ticks(22))
																									sim23 = 100.00 - (if (value < 0) -value else value)
																									if (sim23 > 50) {
																										value = getPercentChange(pattern(23), ticks(23))
																										sim24 = 100.00 - (if (value < 0) -value else value)
																										if (sim24 > 50) {
																											value = getPercentChange(pattern(24), ticks(24))
																											sim25 = 100.00 - (if (value < 0) -value else value)
																											if (sim25 > 50) {
																												value = getPercentChange(pattern(25), ticks(25))
																												sim26 = 100.00 - (if (value < 0) -value else value)
																												if (sim26 > 50) {
																													value = getPercentChange(pattern(26), ticks(26))
																													sim27 = 100.00 - (if (value < 0) -value else value)
																													if (sim27 > 50) {
																														value = getPercentChange(pattern(27), ticks(27))
																														sim28 = 100.00 - (if (value < 0) -value else value)
																														if (sim28 > 50) {
																															value = getPercentChange(pattern(28), ticks(28))
																															sim29 = 100.00 - (if (value < 0) -value else value)
																															if (sim29 > 50) {
																																value = getPercentChange(pattern(29), ticks(29))
																																sim30 = 100.00 - (if (value < 0) -value else value)

																																val howSim = (sim1 + sim2 + sim3 + sim4 + sim5 + sim6 + sim7 + sim8 + sim9 + sim10
																																	+ sim11 + sim12 + sim13 + sim14 + sim15 + sim16 + sim17 + sim18 + sim19 + sim20
																																	+ sim21 + sim22 + sim23 + sim24 + sim25 + sim26 + sim27 + sim28 + sim29 + sim30) / 30.0

																																if (howSim > 70) {
																																	//val patternIndex = patterns.indexWhere(_ == pattern)
																																	patternFound = true
																																	matchingPatterns.append(pattern)
																																}

																															}
																														}
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			val howSim2 = (sim1 + sim2 + sim3 + sim4 + sim5 + sim6 + sim7 + sim8 + sim9 + sim10
				+ sim11 + sim12 + sim13 + sim14 + sim15 + sim16 + sim17 + sim18 + sim19 + sim20
				+ sim21 + sim22 + sim23 + sim24 + sim25 + sim26 + sim27 + sim28 + sim29 + sim30) / 30.0

			howSims += howSim2

		})

		//println("howSimsCount=" + howSims.length)
		//println("howSimsSum=" + howSims.sum)
		println("Pattern loop took: " + (System.currentTimeMillis() - startTime) / 1000.0)
		val start2Time = System.currentTimeMillis()

		val predictions = new ArrayBuffer[PrecisionType]
		if (patternFound) {

			matchingPatterns.foreach(matchingPattern => {
				val futurePoints = patterns.indexWhere(_ == matchingPattern)

				if (performances(futurePoints) > ticks(29)) {
					//pcolor = '# 24 bc00 '
					predictions.append(1.0)
				}
				else {
					//pcolor = '# d40000 '
					predictions.append(-1.0)
				}
				predictedOutcomes.append(performances(futurePoints))
			})

			println("Pattern future loop took: " + (System.currentTimeMillis() - start2Time) / 1000.0)

			val realOutcomeRange = allAveragePrices.slice(intersect + 20, intersect + 30)

			if (realOutcomeRange.nonEmpty) {

				val realAvgOutcome = realOutcomeRange.sum / realOutcomeRange.length

				//val predictedAvgOutcome = predictedOutcomes.sum / predictedOutcomes.length

				val realMovement = getPercentChange(allAveragePrices(intersect), realAvgOutcome)

				val predictionAverage = predictions.sum / predictions.length

				if (predictionAverage < 0) {
					if (realMovement < ticks(29)) {
						accuracies.append(100)
					}
					else {
						accuracies.append(0)
					}
				}

				if (predictionAverage > 0) {
					if (realMovement > ticks(29)) {
						accuracies.append(100)
					}
					else {
						accuracies.append(0)
					}

				}

			}

		}

		println("Pattern patternRecognition took: " + (System.currentTimeMillis() - startTime) / 1000.0)
		predictedOutcomes
	}


}

}