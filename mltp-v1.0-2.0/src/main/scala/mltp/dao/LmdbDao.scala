// Author: Jacek Aleksander Gruca

package mltp.dao

import org.fusesource.lmdbjni.Constants._
import org.fusesource.lmdbjni._

class LmdbDao(dbPath: String) extends DataAccessObject {

	val env = new Env(dbPath)
	val LmdbRecommendedMapSize = 4294967296l
	env.setMapSize(LmdbRecommendedMapSize)

	val db = env.openDatabase()
	var tx: Transaction = null

	override def savePair(key: String, value: String) = {
		db.put(bytes(key), bytes(value))
	}

	override def getValue(key: String): String = {
		string(db.get(bytes(key)))
	}

	override def cleanup() = {
		db.close()
		env.close()
	}

	override def openTransaction() = {
		if (tx != null) {
			println("Finish open transaction first.")
			sys.exit(-1)
		} else {
			tx = env.createWriteTransaction()
		}
	}

	override def closeTransaction() = {
		tx.commit()
		tx = null
	}

	override def savePairInsideTransaction(key: String, value: String) = {
		if (tx == null) {
			println("Transaction not open.")
			sys.exit(-1)
		}
		db.put(tx, bytes(key), bytes(value))
	}

}
