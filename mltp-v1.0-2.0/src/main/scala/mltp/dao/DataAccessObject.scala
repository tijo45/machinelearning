// Author: Jacek Aleksander Gruca

package mltp.dao

trait DataAccessObject {

	def cleanup()

	def getValue(key: String): String

	def savePair(key: String, value: String)

	def openTransaction()

	def closeTransaction()

	def savePairInsideTransaction(key: String, value: String)

}
