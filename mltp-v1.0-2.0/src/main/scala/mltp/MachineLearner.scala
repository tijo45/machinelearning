// Author: Jacek Aleksander Gruca

package mltp

import mltp.dao._
import mltp.stores.{MatchPatternsStore, StorePatternsStore}

import scala.collection.mutable._
import scala.io._

object MachineLearner {

	var debug = false

	val toWhat = 61950
	val usage =
		"""
Run as:
mltp.MachineLearner -input-file <input file path> -db-path <db path>
		"""

	val IntersectKey = "ml-intersect"

	type PrecisionType = Double
	type OptionMap = Map[Symbol, Any]
	type Pattern = ArrayBuffer[PrecisionType]

	def nextOption(map: OptionMap, list: List[String]): OptionMap = {
		list match {
			case Nil => map
			case "-input-file" :: value :: tail =>
				nextOption(map ++ Map('inputfile -> value), tail)
			case "-db-path" :: value :: tail =>
				nextOption(map ++ Map('dbpath -> value), tail)
			case "--debug" :: tail =>
				nextOption(map ++ Map('debug -> true), tail)
			case option :: tail => println("Unknown option " + option)
				println(usage)
				sys.exit(0)
		}
	}

	def main(args: Array[String]): Unit = {

		val totalStartTimeMillis = System.currentTimeMillis()

		val options = nextOption(Map(), args.toList)
		if (options.size < 2) {
			println(usage)
			sys.exit(0)
		}
		val src = Source.fromFile("" + options('inputfile))
		val lines = src.getLines().map(_.split(","))
		if (options.contains('debug)) {
			debug = true
		}

		val date = new ArrayBuffer[String]()
		val bid = new ArrayBuffer[PrecisionType]()
		val ask = new ArrayBuffer[PrecisionType]()

		lines.foreach(a => {
			date += a(0)
			bid += a(1).toDouble
			ask += a(2).toDouble
		})

		val dataLength = bid.length

		val allAveragePrices = new ArrayBuffer[PrecisionType]()

		for (i <- 0 until dataLength) {
			allAveragePrices += (bid(i) + ask(i)) / 2
		}

		val dao = new LmdbDao("" + options('dbpath))
		val storePatternsStore = new StorePatternsStore(dao)
		val matchPatternsStore = new MatchPatternsStore(dao)

		var intersect = toWhat

		val persistedIntersect = if (dao.getValue(IntersectKey) == null) 0 else dao.getValue(IntersectKey).toInt

		while (intersect < dataLength) {

			if (intersect <= persistedIntersect) {
				println("Skipping computation for intersect equal to " + intersect + ".")
				intersect += 1
				//next
			} else {

				val slicedAveragePrices = allAveragePrices.slice(0, intersect)
				PatternProcessor.storePatterns(slicedAveragePrices, storePatternsStore, intersect, dataLength)
				val ticks = PatternProcessor.buildTicks(slicedAveragePrices)
				if (debug) {
					println("ticksSum=" + ticks.sum)
				}

				val predictionOutcomes =
					PatternProcessor.matchPatterns(allAveragePrices, matchPatternsStore, storePatternsStore, intersect, ticks)

				if (debug) {
					println("accuraciesSum=" + matchPatternsStore.getAccuraciesSummed)
					println("predictionOutcomesSum=" + predictionOutcomes.sum)
				}

				var accuracyAverage = 0.0
				var accuracySum = 0.0
				if (matchPatternsStore.getCount > 0) {
					for (i <- 0 to matchPatternsStore.getLastIndex) {
						accuracySum += matchPatternsStore.getAccuracy(i)
					}
					accuracyAverage = accuracySum / matchPatternsStore.getCount
				}

				dao.savePair(IntersectKey, "" + intersect)
				intersect += 1

				println("Array Length " + slicedAveragePrices.length + " " + storePatternsStore.getCount
					+ " " + storePatternsStore.getCount)
				println("Backtested Accuracy is " + accuracyAverage + "% after " + matchPatternsStore.getCount
					+ " actionable trades.")
				if (debug) {
					println("intersect = " + intersect + ", outOf = " + dataLength)
				}
				println("-------------------------------------------------------------------------\n")

			}

		}

		dao.cleanup()

		val totalTimeMillis = (System.currentTimeMillis() - totalStartTimeMillis) / 1000.0

		println("Entire (18) processing took: " + totalTimeMillis + " seconds.")

	}
}
