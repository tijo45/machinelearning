// Author: Jacek Aleksander Gruca

package mltp.stores

import mltp.MachineLearner.PrecisionType
import mltp.dao.DataAccessObject
import java.lang.Double

class MatchPatternsStore(dao: DataAccessObject) {

	val AccuracyPrefix = "mps-accuracy-"
	val IntersectKey = "mps-intersect"
	val LastIndexKey = "mps-last-index"

	val NothingYetLastIndex = -1
	var lastIndex = NothingYetLastIndex
	val NothingYetIntersect = 0
	var intersect = NothingYetIntersect

	def openTransaction() = {
		dao.openTransaction()
	}

	def closeTransaction() = {
		dao.closeTransaction()
	}

	def saveAccuracy(index: Int, accuracy: PrecisionType) = {
		dao.savePairInsideTransaction(AccuracyPrefix + index, "" + accuracy)
	}

	def getAccuracy(index: Int): Double = {
		dao.getValue(AccuracyPrefix + index).toDouble
	}

	def saveLastIndex(indexOfLast: Int) = {
		lastIndex = indexOfLast
		dao.savePairInsideTransaction(LastIndexKey, "" + indexOfLast)
	}

	def getLastIndex: Int = {
		if (lastIndex != NothingYetLastIndex)
			lastIndex
		else if (dao.getValue(LastIndexKey) == null)
			NothingYetLastIndex
		else {
			lastIndex = dao.getValue(LastIndexKey).toInt
			lastIndex
		}
	}

	def getCount = {
		getLastIndex + 1
	}

	def saveIntersect(pIntersect: Int) = {
		intersect = pIntersect
		dao.savePairInsideTransaction(IntersectKey, "" + pIntersect)
	}

	def getIntersect(): Int = {
		if (intersect != NothingYetIntersect)
			intersect
		else if (dao.getValue(IntersectKey) == null)
			NothingYetIntersect
		else {
			intersect = dao.getValue(IntersectKey).toInt
			intersect
		}
	}

	def getAccuraciesSummed = {

		var lSum = 0.0
		for(i <- 0 to getLastIndex) {
			lSum += getAccuracy(i)
		}
		lSum
	}

}
