// Author: Jacek Aleksander Gruca

package mltp.stores

import mltp.MachineLearner._
import mltp.dao.DataAccessObject
import java.lang.Double
import scala.pickling.Defaults._
import scala.pickling.json._

class StorePatternsStore(dao: DataAccessObject) {

	val PatternPrefix = "sps-pattern-"
	val PerformancePrefix = "sps-performance-"
	val StorePatternLastIndexKey = "sps-last-index"
	val YgrekKey = "sps-y"
	val SumKey = "sps-sum"
	val IntersectKey = "sps-intersect"

	val NothingYetIntersect = 0
	var intersect = NothingYetIntersect

	val NothingYetLastIndex = -1
	var lastIndex = NothingYetLastIndex

	val NothingYetYgrek = 0
	var ygrek = NothingYetYgrek

	val NothingYetSum = 0.0
	var sum = NothingYetSum

	def openTransaction() = {
		dao.openTransaction()
	}

	def closeTransaction() = {
		dao.closeTransaction()
	}

	def savePattern(index: Int, pattern: Pattern) = {
		dao.savePairInsideTransaction(PatternPrefix + index, pattern.pickle.value)
	}

	def getPattern(index: Int): Pattern = {
		dao.getValue(PatternPrefix + index).unpickle[Pattern]
	}

	def savePerfromance(index: Int, performance: Double) = {
		dao.savePairInsideTransaction(PerformancePrefix + index, "" + performance)
	}

	def getPerformance(index: Int): PrecisionType = {
		dao.getValue(PerformancePrefix + index).toDouble
	}

	def saveY(y: Int) = {
		ygrek = y
		dao.savePairInsideTransaction(YgrekKey, "" + y)
	}

	def getY(): Int = {
		if (ygrek != NothingYetYgrek)
			ygrek
		else if (dao.getValue(YgrekKey) == null)
			NothingYetYgrek
		else {
			ygrek = dao.getValue(YgrekKey).toInt
			ygrek
		}
	}

	def setLastIndex(indexOfLast: Int) = {
		lastIndex = indexOfLast
		dao.savePairInsideTransaction(StorePatternLastIndexKey, "" + indexOfLast)
	}

	def getLastIndex(): Int = {
		if (lastIndex != NothingYetLastIndex)
			lastIndex
		else if (dao.getValue(StorePatternLastIndexKey) == null)
			NothingYetLastIndex
		else {
			lastIndex = dao.getValue(StorePatternLastIndexKey).toInt
			lastIndex
		}
	}

	def getCount = {
		getLastIndex + 1
	}

	def addSum(suma: Double) {
		if (sum == NothingYetSum)
			sum = getSum
		sum += suma
		dao.savePairInsideTransaction(SumKey, "" + sum)
	}

	def getSum(): Double = {
		if (sum != NothingYetSum)
			sum
		else if (dao.getValue(SumKey) == null)
			NothingYetSum
		else {
			sum = dao.getValue(SumKey).toDouble
			sum
		}
	}

	def saveIntersect(pIntersect: Int) = {
		intersect = pIntersect
		dao.savePairInsideTransaction(IntersectKey, "" + pIntersect)
	}

	def getIntersect(): Int = {
		if (intersect != NothingYetIntersect)
			intersect
		else if (dao.getValue(IntersectKey) == null)
			NothingYetIntersect
		else {
			intersect = dao.getValue(IntersectKey).toInt
			intersect
		}
	}

}
