# README #

This README file helps one get set up with MLTP (Machine Learning for Tick Patterns). MLTP is code developed by JAG (Jacek Aleksander Gruca) for Tijo Varughese as part of their Upwork contract.

### What is this code for? ###

This code performs the analysis of tick patterns and their accuracy based on historical financial data.

### How do I get set up? ###

* Install SBT.

http://www.scala-sbt.org/release/tutorial/Setup.html

SBT is used to build and execute the code.

* Install the LMDB database.

https://lmdb.readthedocs.org/en/release/

LMDB is used to persist the state of computations in between invocations.

* Create a directory for databases.

Suggested to be either /data or c:\data. Don't forget to grant write permissions to it for all.

* Run the code.

Execute the following command:

`sbt "run"`

This will build the project and print its own usage instructions, which are:

`
Run as:
[info] mltp.MachineLearner -input-file <input file path> -db-path <db path>
`

We should, therefore, specify an input file as well the db-path. First, create the appropriate DB location:

`mkdir /data/lmdb1`

Then execute:

`sbt "run -input-file src/main/resources/GBPUSD1d.txt -db-path /data/lmdb1"`

This command will run the computations for file GBPUSD1d.txt persisting their state to /data/lmdb1. If for whatever reason your computation stops abruptly (due to a power cut, hardware failure, etc.) you can restart it by executing the same command as before.

Please note that the db-path should be unique for each input-file.

### Who do I talk to? ###

In case of any problems speak to JAG on jag@gruca.net.
