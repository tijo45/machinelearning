name := "Machine Learning for Tick Patterns"

version := "1.0.0"

fork := true

scalaVersion := "2.11.7"

mainClass in (Compile, run) := Some("mltp.MachineLearner")

libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.4.0"

libraryDependencies += "org.deephacks.lmdbjni" % "lmdbjni-osx64" % "0.4.4"

libraryDependencies += "org.scala-lang.modules" % "scala-pickling_2.11" % "0.10.1"
