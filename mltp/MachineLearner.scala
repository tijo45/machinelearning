// Author: Jacek Aleksander Gruca

package mltp {

import scala.collection.mutable._
import scala.io._

object MachineLearner {

	val toWhat = 55000
	val totalStartTimeMillis = System.currentTimeMillis()
	val usage =
		"""
Run as:
scala mltp.MachineLearner -input-file <input file path>
		"""

	type PrecisionType = Double
	type OptionMap = Map[Symbol, Any]
	type Pattern = ArrayBuffer[PrecisionType]

	def nextOption(map: OptionMap, list: List[String]): OptionMap = {
		list match {
			case Nil => map
			case "-input-file" :: value :: tail =>
				nextOption(map ++ Map('inputfile -> value), tail)
			case option :: tail => println("Unknown option " + option)
				println(usage)
				sys.exit(1)
		}
	}

	def main(args: Array[String]): Unit = {

		if (args.length == 0) {
			println(usage)
			sys.exit(1)
		}

		val argList = args.toList
		val options = nextOption(Map(), argList)
		val src = Source.fromFile("" + options('inputfile))
		val lines = src.getLines().map(_.split(","))

		val date = new ArrayBuffer[String]()
		val bid = new ArrayBuffer[PrecisionType]()
		val ask = new ArrayBuffer[PrecisionType]()

		lines.foreach(a => {
			date += a(0)
			bid += a(1).toDouble
			ask += a(2).toDouble
		})

		val dataLength = bid.length

		val allAveragePrices = new ArrayBuffer[PrecisionType]()

		for (i <- 0 until dataLength) {
			allAveragePrices += (bid(i) + ask(i)) / 2
		}

		val patterns = new ArrayBuffer[Pattern]
		val performances = new ArrayBuffer[PrecisionType]
		val accuracies = new ArrayBuffer[PrecisionType]

		var intersect = toWhat

		while (intersect < dataLength) {

			val slicedAveragePrices = allAveragePrices.slice(0, intersect)
			//println("allAveragePrices="+allAveragePrices.mkString(","))
			//println("avgLine="+slicedAveragePrices.mkString(","))
			PatternProcessor.storePatterns(slicedAveragePrices, patterns, performances)
			val ticks = PatternProcessor.buildTicks(slicedAveragePrices)
			//println("ticks=" + ticks.mkString(","))
			//println("ticksSum=" + ticks.sum)

			//val predictionOutcomes =
			PatternProcessor.matchPatterns(ticks, patterns, performances, allAveragePrices, intersect, accuracies)

			//println("accuracies=" + accuracies.mkString(","))
			//println("accuraciesSum=" + accuracies.sum)
			//println("predictionOutcomes=" + predictionOutcomes.mkString(","))
			//println("predictionOutcomesSum=" + predictionOutcomes.sum)

			var accuracyAverage = 0.0
			if (accuracies.nonEmpty) {
				accuracyAverage = accuracies.sum / accuracies.length
			}

			intersect += 1

			println("Array Length " + slicedAveragePrices.length + " " + patterns.length + " " + performances.length)
			println("Backtested Accuracy is " + accuracyAverage + "% after " + accuracies.length + " actionable trades.")
			//println("intersect = " + intersect + ", outOf = " + dataLength)

		}

		val totalTimeMillis = (System.currentTimeMillis() - totalStartTimeMillis) / 1000.0

		println("Entire (18) processing took: " + totalTimeMillis + " seconds ")

	}
}

}